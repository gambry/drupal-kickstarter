.SILENT:

.PHONY: help list

help: ## Lists all documented Make targets.
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m\make %-30s\033[0m %s\n", $$1, $$2}'
list: help

install: up composer-up drupal-site-install login ## Install project dependencies (creates scaffolding the first time it runs)
uninstall: docker-rm  ## Stop & Remove Project
up: docker-up ## Spin up an already installed project
login: drush-uli ## Get User Login Link
tests: code-analysis phpunit ## Run Tests

composer-up: composer-install

composer-install:
	ddev composer install

drush-uli:
	ddev drush uli

docker-up:
	if ! [ -e .ddev ]; then \
		ddev config; \
		ddev config --omit-containers=dba --php-version=8.1; \
	fi;
	ddev start

docker-rm:
	ddev stop
	ddev remove

cc: ## Clear cache(s)
	ddev drush cr

drupal-site-install:
	if ! [ -e web/sites/default/settings.local.php ]; then \
		echo "Creating settings.local.php"; \
		cp web/sites/example.settings.local.php web/sites/default/settings.local.php; \
	fi;
	ddev drush si -y minimal

code-analysis:
	ddev exec phpcs

phpunit:
	ddev exec mkdir -p web/sites/default/files/simpletest/browser_output
	ddev exec 'SIMPLETEST_DB=$$(drush php:eval "print \Drupal\Core\Database\Database::getConnectionInfoAsUrl()") SIMPLETEST_BASE_URL=http://127.0.0.1/ bash -c "vendor/bin/phpunit -c web/core/ web/modules/custom"'

seed-db: ## Initialise local database with remote data.
#	ddev drush sql:sync @site.dev @self
	echo "No database seed script found."
