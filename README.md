# Drupal Kickstarter

A Drupal kick-starter with local dockerized host, GNU Make as tech-agnostic task runner, Drupal TOP recommended modules and setup.

## Requirements
1. [Docker](https://docs.docker.com/get-docker/) (Since Docker Desktop moved to a paid subscription, [check some alternatives](https://ddev.com/ddev-local/docker-desktop-alternatives-arrive-for-ddev-colima/))
2. [DDEV](https://ddev.readthedocs.io/en/stable/)
3. Git [optional, but recommended]
4. GNU Makefile utility [optional, but recommended]
5. Composer [optional, but recommended]

## Get Started

### Why do I need this?
You don't.
What this kick-starter does is:
- Setup DDev as local host environment
- Create a project following latest drupal/recommended-project
- Requires other important packages like drush/drush or drupal/core-dev
- Prepare Drupal codebase with common settings
- Give an easy onboard, setting up the project locally with `make install`, `make login`, `make seed-db`
- Already setup for PHPCS and PHPUnit tests with Drupal with `make tests`

So you can do all of these steps manually, all the times. Or just use this project.

### Usage

1. Run `composer create-project gambry/drupal-kickstarter my-project`
2. `cd my-project`
3. Now run `make install` and you are ready to go.

## Tools & Utilities

### Makefile
We use Makefile as task runner, to be truly technology-agnostic. This could be a Drupal, or PHP or JS project; running
within a hosted LAMP stack, with Vagrant, Docker, K8, etc. For what it matters `make install` setups the project and
`make login` gives you a unique link to login to the web platform.

Run `make help` to know all the incredible shortcuts you may need.
